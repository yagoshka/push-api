var extend = require('extend'),
    config = {};

config.env = process.env.NODE_ENV || 'dev';

switch (config.env) {
    case 'production':
        extend(config, require('./config.prod.js'));
        break;

    default:
        extend(config,  require('./config.dev.js'));
}

console.log(JSON.stringify(config));

module.exports = config;