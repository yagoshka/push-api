module.exports = {
    staticDir: '.',
    port: Number(process.env.PORT || 5000)
};