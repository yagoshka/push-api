(function(){
    function push(data) {
        var recent = store.get(this.storeName),
            me = this;
        recent? $.each(recent, function (index, item) {
            if (item && item[me.storeKey] === data[me.storeKey]) {
                recent.splice(index, 1);
            }
        }) : (recent = []);
        recent.push(data);

        if (this.limit) {
            recent = recent.splice(-1 * this.limit, this.limit);
        }

        store.set(this.storeName, recent);
        $(window).trigger('store:' + this.storeName + ':change');
    }

    function pop() {
        $(window).trigger('store:' + this.storeName + ':change');
    }

    function all() {
        var a = store.get(this.storeName) || [];
        return a.reverse();
    }

    function clear() {
        store.set(this.storeName, []);
        $(window).trigger('store:' + this.storeName + ':change');
    }

    var R = function(store_name, params) {
        var me = this,
            events = 'change,clear';
        this.storeName = store_name;
        this.storeKey = params.key || 'id';

        if (store.get(this.storeName) === undefined) {
            store.set(this.storeName, []);
        }

        return {
            push: function(data) {
                push.call(me, data);
            },
            pop: pop,
            all: function(){
                return all.call(me);
            },
            clear: function() {
                clear.call(me);
            },
            onChange: function(callback) {
                $(window).on('store:' + me.storeName + ':change', function() {
                    callback.call(me, all.call(me));
                });
            }
        };
    };

    window.Recent = R;
})();