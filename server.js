var EXPRESS = require('express'),
    app = EXPRESS(),
    request = require('request'),
    bodyParser = require('body-parser'),
    compression = require('compression'),
    config = require('./config'),
    URI = require('URIjs'),
    urlencode = require('urlencode'),
    objectSort = require('sort-object'),
    extend = require('extend'),
    sha1 = require('crypto').createHash('sha1');

function calculateSig(data, api_key) {
    var sig = '';
    data = objectSort(data);
    for (var key in data) {
        if (data.hasOwnProperty(key)) {
            sig += data[key];
        }
    }
    sig = require('crypto').createHash('sha1').update(sig + api_key).digest('hex');

    return sig;
}

URI.escapeQuerySpace = false;

//app.use( bodyParser.json() );       // to support JSON-encoded bodies
app.use( bodyParser.urlencoded() ); // to support URL-encoded bodies
app.use(compression());

app.use(function(err, req, res, next){
    console.error(err);
    res.send(500);
});

app.use('/js', EXPRESS.static(__dirname + '/' + config.staticDir + '/js'));
app.use('/css', EXPRESS.static(__dirname + '/' + config.staticDir + '/css'));
app.use('/images', EXPRESS.static(__dirname + '/' + config.staticDir + '/images'));

app.get('/', function(req, res) {
    res.sendfile(config.staticDir + '/index.html');
});

app.post('/send', function(req, res) {
    var data = req.body,
        api_key = data.api_key,
        error = [],
        url = 'https://store.icq.com/openstore/push';

    if (!data.api_key) {
        error.push('NO API_KEY');
    }

    if (!data.uin) {
        error.push('NO UIN');
    }

    if (!data.app_id) {
        error.push('NO APP_ID');
    }

    if (!data.push_text) {
        error.push('NO PUSH_TEXT');
    }

    if (error.length) {
        res.send({
            error: error.join(', ')
        });
        return false;
    }

    data.client = 'icq';

    data.data = data.push_data;

    try {
        data.data = JSON.stringify(JSON.parse(data.data));
    } catch(e) {}

    data.data = encodeURIComponent(data.data);
    data.push_text = urlencode(data.push_text);

    delete data.push_data;
    delete data.api_key;

    data.signature = calculateSig(data, api_key);
    url = URI(url).query(data).toString();

    request.post(url, function(err, r, body) {
        if (err) {
            throw new Error(err);
            return false;
        }

        res.send({
            timestamp: +new Date(),
            request_params: data,
            request_url: url,
            response: JSON.parse(body)
        });
    })

});

app.listen(config.port);

